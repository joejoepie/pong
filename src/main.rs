use amethyst::{
    core::transform::TransformBundle,
    input::{InputBundle, StringBindings},
    network::simulation::laminar::{LaminarNetworkBundle, LaminarSocket},
    prelude::*,
    renderer::{
        plugins::{RenderFlat2D, RenderToWindow},
        types::DefaultBackend,
        RenderingBundle,
    },
    utils::application_root_dir,
};

use std::env;

mod ball;
mod paddle;
mod pong;
mod server;

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let app_root = application_root_dir()?;

    let assets_dir = app_root.join("assets");
    let config_dir = app_root.join("config");
    let display_config_path = config_dir.join("display.ron");

    let socket = LaminarSocket::bind("0.0.0.0:3457")?;

    let game_data = GameDataBuilder::default()
        .with_bundle(
            RenderingBundle::<DefaultBackend>::new()
                .with_plugin(
                    RenderToWindow::from_config_path(display_config_path)?
                        .with_clear([0.34, 0.36, 0.52, 1.0]),
                )
                .with_plugin(RenderFlat2D::default()),
        )?
        .with_bundle(TransformBundle::new())?
        .with_bundle(
            InputBundle::<StringBindings>::new()
                .with_bindings_from_file(config_dir.join("bindings.ron"))?,
        )?
        .with_bundle(LaminarNetworkBundle::new(Some(socket)))?
        .with(paddle::PaddleSystem, "paddle_system", &["input_system"])
        .with(ball::BallMoveSystem, "ball_move_system", &["input_system"])
        .with(
            ball::BallBounceSystem,
            "ball_bounce_system",
            &["input_system", "ball_move_system"],
        )
        .with_system_desc(server::PongServerDesc, "pong_server", &[]);

    if let Some(param) = env::args().nth(1) {
        match &param[..] {
            "--server" => {}
            _ => {}
        }
    }

    let mut game = Application::new(assets_dir, pong::PongState, game_data)?;
    game.run();

    Ok(())
}
