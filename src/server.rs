use amethyst::{
    core::SystemDesc,
    ecs::{Read, System, SystemData, World, Write},
    network::simulation::{NetworkSimulationEvent, TransportResource},
    shrev::{EventChannel, ReaderId},
};

#[derive(Debug)]
pub struct PongServer {
    reader: ReaderId<NetworkSimulationEvent>,
}

impl PongServer {
    fn new(reader: ReaderId<NetworkSimulationEvent>) -> Self {
        Self { reader }
    }
}

pub struct PongServerDesc;

impl<'a, 'b> SystemDesc<'a, 'b, PongServer> for PongServerDesc {
    fn build(self, world: &mut World) -> PongServer {
        // Creates the EventChannel<NetworkEvent> managed by the ECS.
        <PongServer as System<'_>>::SystemData::setup(world);
        // Fetch the change we just created and call `register_reader` to get a
        // ReaderId<NetworkEvent>. This reader id is used to fetch new events from the network event
        // channel.
        let reader = world
            .fetch_mut::<EventChannel<NetworkSimulationEvent>>()
            .register_reader();
        PongServer::new(reader)
    }
}

impl<'s> System<'s> for PongServer {
    type SystemData = (
        Write<'s, TransportResource>,
        Read<'s, EventChannel<NetworkSimulationEvent>>,
    );
    fn run(&mut self, (mut net, channel): Self::SystemData) {
        for event in channel.read(&mut self.reader) {
            match event {
                NetworkSimulationEvent::Connect(addr) => println!("New client connected: {}", addr),
                _ => {}
            }
        }
    }
}
